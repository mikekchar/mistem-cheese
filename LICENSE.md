# License

![CC0]("http://i.creativecommons.org/p/zero/1.0/88x31.png")

[CC0](http://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, Mike Charlton has waived all
copyright and related or neighboring rights to
[Mistem Cheese](https://gitlab.com/mikekchar/mistem-cheese).
This work is published from: Japan.

Note that images in the directory
(https://gitlab.com/mikekchar/mistem-cheese/public/images)
are not covered in this license.  Those images are copyrighted by
their respective owners.  They are used on this specific site with
permission.
