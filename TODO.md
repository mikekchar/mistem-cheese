# Mistem's Cheese Blog TODO

# Clean up

  - [ ] The License is wrong in the main repository
  - [ ] The Readme is wrong in the main repository
  - [ ] Write a yogurt blog post
  - [ ] There are no RSS links in the sub-pages of Mistem's cheese
  - [ ] Need an explanation of how to use RSS to subscribe

# Branch: what-is-cheese

  - [X] Add a new article
  - [X] Get a banner picture (From Wikipedia)
  - [X] Add to contents
  - [X] Create a "latest" section
  - [X] Initial feed entry
  - [X] Add RSS to article
  - [X] Write article
  - [X] Spell check (what?)
    - Wow.  vim - `:set spell` FTW
  - [X] Update feed
