# Mistem Cheese

This is a static website the purpose of which is to help people learn
how to make cheese even if they don't have access to rennet, or
specialised equipment.  I don't actually know much about cheese
making, but for some reason I set myself the task of making a good
tasting, aged cheese, without rennet and with a minimum of equipment
(no press, no specialised cheese fridge, no fancy molds, etc).

To be fair, it's a bit like doing everything wrong, but still trying
to have a good result.  In that way it's playing on hard mode.  I
wonder if it's actually a good way for beginners... Having said that,
I'm a beginner and I had a blast doing it, so why not?  I guess the
best way of saying it is that it's for those who are poor, stingy or
just like an unreasonable challenge.

The name "Mistem" comes from the web comic written pseudonymously by
evictedSaint on the Dwarf Fortress forums.  Mistem Wheeldream (or
Misty, for short) seeks to make her way to a new dwarf fortress in
order to follow her dream of becoming a legendary cheesemaker.  There
is very little actual cheese making in the web comic, but its cuteness
inspired me to get off my butt and try to make cheese myself.  In any
case, I highly recommend you have a read if you are in the mood:
[The Littlest Cheesemaker](http://www.bay12forums.com/smf/index.php?topic=136384.0)

I'm currently hosting this website on GitLab.
[Click here](https://mikekchar.gitlab.io/mistem-cheese) to see the
main site.  Because I'm poor(ish) and stingy(not really ish) I needed
a place to statically host this for free.  I also wanted to check out
GitLab, so this was a good opportunity.  Thanks to GitLab for
providing this service.

## License

![alt text](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")

[CC0](http://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, Mike Charlton has waived all
copyright and related or neighboring rights to
[Mistem Cheese](https://gitlab.com/mikekchar/mistem-cheese).
This work is published from: Japan.

Note that images in the directory
(https://gitlab.com/mikekchar/mistem-cheese/public/images)
are not covered in this license.  Those images are copyrighted by
their respective owners.  They are used on this specific site with
permission.
