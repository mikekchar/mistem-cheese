<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>What Is Cheese?</title>
    <link rel="stylesheet" href="../styles/mistem.css">
    <link type="application/atom+xml" rel="alternate" href="../feed.xml"/>
  </head>
  <body>
    <header>
      <div class="navigation">
        <a class="feed" type="application/atom+xml"
           rel="alternate" href="../feed.xml"
           title="Download RSS Feed">
          <img alt="RSS Feed Icon" src="../images/Feed-icon.svg"/>
        </a>
        <a href="../index.html">Mistem's Cheese Blog</a>
      </div>
      <h1>What Is Cheese?</h1>
      <aside>A rose by any other name...</aside>
    </header>
    <p>
      <img alt="Slice of Emmentaler Swiss Cheese" class="banner"
           srcset="../images/what-is-cheese/cheese-320w.jpg 320w,
                   ../images/what-is-cheese/cheese-480w.jpg 480w,
                   ../images/what-is-cheese/cheese-800w.jpg 800w"
           sizes="(max-width: 320px) 320px,
                  (max-width: 480px) 480px,
                  800px"
           src="../images/what-is-cheese/cheese-800w.jpg">
    </p>

    <h2>The importance of words</h2>
    <p>
      I don't suppose anybody who comes to this blog will do so trying to
      figure out what cheese is.  We all have experience with it and we
      all have a pretty good idea of what it is.  However, words and their
      definitions are are tricky.  As humans, we use words to model the
      world.  When we are thinking, and planning, and solving puzzles, we
      normally do so with a running dialog of words in our brains.  Frequently
      we don't question the definitions of our words and so bias creeps
      into our thinking.
    </p>
    <p>
      It's not really the case that any given definition is better than another, in
      general.  If you can manage to communicate what you want to say, then
      it doesn't matter which words you choose or how you use them.  However,
      the choice still influences our thinking and so one can say that some
      word choices are better for specific circumstances.
    </p>
    <p>
      Wikipedia defines "cheese" as:
      <q>a dairy product produced in wide ranges
        of flavors, textures, and forms by coagulation of the milk protein
        casein.</q>
      I think most people have an image of cheese similar to the
      banner image on this page.  As Wikipedia says, though, cheese has such a
      wide variety of shapes, textures and flavors that it's often not useful
      to define it in those terms.  Instead, the important part is that it is
      made from coagulated milk protein.
    </p>

    <h2>Protein</h2>
    <p>
      Casein is a type of protein.  It exists in milk in tightly wound bundles,
      called micelles.  Proteins are large chains of molecular structures.  By
      and large, these chains are made up of carbon, nitrogen, hydrogen and oxygen
      atoms in a variety of different combinations.  There are a mind bogglingly
      large number of different combinations that are viable.
    </p>
    <p>
      It's easy to think that "protein is protein", but that isn't the case.
      The combination of the molecular structures and their shape matters.
      Consider 2 pieces of paper.  They are both paper.  You can fold one
      into a paper airplane and another into an origami crane.  Nothing has
      changed about the paper, but these two pieces now have very different
      properties.  For example the paper plane can fly while, despite having
      wings, the paper crane can not.  I like to think about proteins in the
      same way.  Each different shape gives it completely different properties.
      Casein has different properties from any other protein, and this is
      why we can uniquely make cheese from it.
    </p>
    <p>
      Proteins are composed in pieces.  The smallest piece is called an
      "amino acid".  While there are just over 500 amino acids in nature,
      there are really only about 21 that occur in proteins.  These amino
      acids hang off of larger structures called "peptides".  As you can
      imagine, there are a massive number of combinations of peptides that
      you can construct from the original 21 amino acids.  Finally, proteins
      are made up of peptides.
    </p>
    <p>
      When we age cheese, the casein in the cheese breaks down into peptides
      and then finally into amino acids.  Protein itself doesn't have a
      strong flavor or odor, but peptides and amino acids <em>do</em>.  I
      like to say that peptide flavors can run from rubber to roses and
      everything in between.  We will talk another time about how the
      proteins break down into these pieces.  The key point, though, is that
      if you don't start with casein, you will not get the same peptides as
      you break down the protein.  This is why you can't get cheese flavours
      from non-dairy proteins.
    </p>
    <p>
      If you are having trouble imagining this, consider the case of our
      paper plane and paper crane.  Imagine that we took some scissors and
      cut them up.  Then imagine that each different shape of paper had
      a different taste and smell.  No matter how carefully you cut the
      plane and crane up, it would be impossible to end up with pieces
      of paper of the same shape.  The plane will taste dramatically
      different than the crane in the end.
    </p>

    <h2>Where do we go from here?</h2>
    <p>
      One of the reasons I wanted to talk about the definition of cheese is that
      I want to make a very easy cheese: yogurt.  Your common sense may tell you
      that yogurt is not a cheese!  However, it fits our definition just fine.
      It is formed from the coagulation of casein in milk.  I like to call this
      type of cheese a "fresh, unsalted, undrained, thermophilic lactic cheese"...
      which... is a mouthful.  Next time, I will present a simple "recipe" for
      yogurt and talk about some of the things we can do with it.
    </p>

    <footer>
      <div class="navigation">
        <a class="feed" type="application/atom+xml"
           rel="alternate" href="../feed.xml"
           title="Download RSS Feed">
          <img alt="RSS Feed Icon" src="../images/Feed-icon.svg"/>
        </a>
        <a href="../index.html">Mistem's Cheese Blog</a>
      </div>
      <p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#" class="license-text">
        <span rel="dct:title">Mistem's Cheese Blog</span> by
        <span property="cc:attributionName">Mike Charlton</span>
        is licensed under
        <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0">
          CC BY-SA 4.0
          <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
               src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" />
          <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
               src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" />
          <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
               src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1" />
        </a>
      </p>
      <p class="license-text">
        Cheese image is public domain:
        <a href="https://commons.wikimedia.org/wiki/File:NCI_swiss_cheese.jpg">Wikipedia</a>
        <br>
        Feed icon is GPL:
        <a href="https://en.wikipedia.org/wiki/File:Feed-icon.svg">Wikipedia</a>
      </p>
    </footer>
  </body>
</html>
